<?php

namespace Drupal\jsonapi_operations\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\jsonapi\JsonApiResource\ErrorCollection;
use Drupal\jsonapi\JsonApiResource\IncludedData;
use Drupal\jsonapi\JsonApiResource\JsonApiDocumentTopLevel;
use Drupal\jsonapi\JsonApiResource\LinkCollection;
use Drupal\jsonapi\JsonApiResource\NullIncludedData;
use Drupal\jsonapi\JsonApiResource\ResourceObjectData;
use Drupal\jsonapi\ResourceResponse;
use Drupal\jsonapi_operations\Exception\OperationException;
use JsonPath\JsonObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Bulk operations front controller.
 * Bulk operations front controller.
 *
 * @package Drupal\jsonapi_operations\Controller
 */
class OperationsHandler {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var \Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface */
  protected $resourceTypeRepository;

  /** @var \Drupal\Core\Database\Connection */
  protected $databaseConnection;

  /** @var \Symfony\Component\HttpKernel\HttpKernelInterface */
  protected $httpKernel;

  /**
   * Construct a new OperationsHandler.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   the Drupal entity type manager
   * @param $resourceTypeRepository
   *   the JSON:API resource type repository
   * @param $databaseConnection
   *   the connection to the database
   * @param $httpKernel
   *   the Drupal http kernel
   */
  public function __construct($entityTypeManager, $resourceTypeRepository, $databaseConnection, $httpKernel) {
    $this->entityTypeManager = $entityTypeManager;
    $this->resourceTypeRepository = $resourceTypeRepository;
    $this->databaseConnection = $databaseConnection;
    $this->httpKernel = $httpKernel;
  }

  /**
   * Handles JSON:API Operations incoming HTTP requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   the HTTP request
   *
   * @return \Drupal\jsonapi\ResourceResponse|\Symfony\Component\HttpFoundation\Response
   *   the HTTP response
   */
  function handleOperations(Request $request) {


    $body = Json::decode($request->getContent());
    $operations = $body['operations'];

    if(!$operations || !is_array($operations)){
      $jsonApiDocument = new JsonApiDocumentTopLevel(new ResourceObjectData([]), new IncludedData([]), new LinkCollection([]));
      return new ResourceResponse($jsonApiDocument, 400);
    }

    $transaction = $this->databaseConnection->startTransaction();

    try {

      $responses = array_reduce($operations, function ($carry, $operation) use ($request, $transaction) {

        try {
          $op = $operation['op'];
          $data = !empty($operation['data']) ? $operation['data'] : NULL;
          $type = !empty($operation['ref']['type']) ? $operation['ref']['type'] : NULL;
          $id = !empty($operation['ref']['id']) ? $operation['ref']['id'] : NULL;

          $resourceType = $this->resourceTypeRepository->getByTypeName($type);
          if (!$resourceType) {
            throw new HttpException(400, 'No type provided for the operation.');
          }
          $storage = $this->entityTypeManager->getStorage($resourceType->getEntityTypeId());

          $data = self::replaceTokens($data, $carry);

          if ($id) {
            $entities = $storage->loadByProperties(['uuid' => $id]);
            $entity = $entities && count($entities) ? reset($entities) : NULL;
          }
          if ($entity) {
            $url = Url::fromRoute(sprintf('jsonapi.%s.individual', $type), ['entity' => $entity->uuid()]);
          }
          else {
            $url = Url::fromRoute(sprintf('jsonapi.%s.collection', $type));
          }
          $content = NULL;
          switch ($op) {
            case 'get':
              $method = 'GET';
              break;
            case 'add':
            case 'update':
              $content = Json::encode([
                'data' => $data,
              ]);
              if ($op === 'update') {
                $method = 'PATCH';
              }
              else {
                $method = 'POST';
              }
              break;
            case 'delete':
            case 'remove':
              $method = 'DELETE';
              break;

            default:
              throw new HttpException(400, 'Unsupported operation: ' . $op);
          }

          $subRequest = Request::create($url->toString(TRUE)->getGeneratedUrl(), $method, [], $request->cookies->all(), $request->files->all(), $request->server->all(), $content);
          $responseItem = [
            'operation' => $operation,
            'response' => $this->httpKernel->handle($subRequest, HttpKernelInterface::MASTER_REQUEST, FALSE),
          ];
          $carry = array_merge($carry, [$responseItem]);
        }catch(\Exception $exception){
          throw new OperationException(count($carry), $exception);
        }
        return $carry;
      }, []);


      $resource_objects = array_map(function ($responseItem) {
        return Json::decode($responseItem['response']->getContent());
      }, $responses);

      return new Response(Json::encode(['operations' => $resource_objects]));

    }catch(\Exception $exception){
      $transaction->rollBack();
      $meta = [];
      if($exception instanceof OperationException){
        $meta['operation'] = [
          'index' => $exception->getOperationIndex(),
        ];
        $exception = $exception->getPrevious();
      }
      if (!$exception instanceof HttpException) {
        $exception = new HttpException(500, $exception->getMessage(), $exception);
      }
      return new ResourceResponse(new JsonApiDocumentTopLevel(new ErrorCollection([$exception]), new NullIncludedData(), new LinkCollection([]), $meta), $exception->getStatusCode(), $exception->getHeaders());
    }

  }

  protected static function replaceTokens($data, $carry){
    $dataBlob = json_encode($data);
    $pattern = '/\{\{([^\{\}]+\.[^\{\}]+)@([^\{\}]+)\}\}/';
    if (preg_match_all($pattern, $dataBlob, $matches)) {
      for ($index = 0; $index < count($matches[0]); $index++) {
        // We only care about the first three items: full match, subject ID and
        // JSONPath query.
        $output[] = [
          $matches[0][$index],
          $matches[1][$index],
          $matches[2][$index]
        ];
      }
      foreach ($output as $o){
        list($reqId, $source) = explode('.',$o[1]);
        if($source != 'body'){
          throw new \Exception('Only \'body\' source is supported');
        }

        $candidateRequests = array_filter($carry, function($item) use ($reqId){
          return !empty($item['operation']['meta']['requestId']) && $item['operation']['meta']['requestId'] == $reqId;
        });
        if(count($candidateRequests) < 1){
          throw new \Exception('no previous request found for id ' . $reqId);
        }
        if(count($candidateRequests) > 1){
          throw new \Exception('more than one requests found for id ' . $reqId);
        }

        $request = reset($candidateRequests);

        /** @var Response $resp */
        $resp  = $request['response'];
        $json_object = new JsonObject($resp->getContent());
        $to_replace = $json_object->get($o[2]) ?: [];
        $dataBlob = static::replaceTokenSubject($o[0], $to_replace[0], $dataBlob);
      }
      return json_decode($dataBlob);
    }
    return $data;

  }

  protected static function replaceTokenSubject($token, $value, $token_subject) {
    // Escape regular expression.
    if (is_int($value) || is_float($value) || is_bool($value)) {
      if (is_bool($value)) {
        $value = $value ? 'true' : 'false';
      }
      $regexp = sprintf('/%s/', preg_quote("\"$token\""), '/');
      $token_subject = preg_replace($regexp, $value, $token_subject);
    }
    $regexp = sprintf('/%s/', preg_quote($token), '/');
    return preg_replace($regexp, $value, $token_subject);
  }

}
