<?php

namespace Drupal\jsonapi_operations\Exception;

/**
 * Wraps exceptions triggered by the execution of one operation.
 *
 * Provides some additional metadata such as the index of the operations
 * which triggered the error.
 */
class OperationException extends \Exception {

  /**
   * The operation index.
   *
   * @var int
   */
  protected $operationIndex;

  /**
   * OperationException constructor.
   *
   * @param int $operationIndex
   *   the index of the operation that triggered the error
   * @param \Throwable|NULL $previous
   *   the original triggered error
   */
  public function __construct($operationIndex, \Throwable $previous = NULL) {
    parent::__construct("Operation exception", 0, $previous);
    $this->operationIndex = $operationIndex;
  }

  /**
   * Returns the operation index.
   *
   * @return int
   *   the operation index
   */
  public function getOperationIndex() {
    return $this->operationIndex;
  }
}
