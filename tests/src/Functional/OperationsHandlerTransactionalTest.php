<?php

namespace Drupal\Tests\jsonapi_operations\Functional;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\Tests\jsonapi\Functional\JsonApiFunctionalTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * @group jsonapi_operations
 */
class OperationsHandlerTransactionalTest extends JsonApiFunctionalTestBase {

  public static $modules = [
    'jsonapi_operations',
  ];

  protected function setUp() {
    parent::setUp();
    $this->grantPermissions(Role::load(RoleInterface::ANONYMOUS_ID), [
      'create article content',
      'edit any article content',
      'delete any article content',
    ]);
  }

  function handleOperations(array $payload){
    $operations_url = Url::fromRoute('jsonapi_operations.operations_handler');
    return $this->request('PATCH', $operations_url, [
      'body' => Json::encode($payload),
      'headers' => ['Content-Type' => 'application/vnd.api+json'],
    ]);
  }

  function testTwoAddsSecondFails() {

    $this->config('jsonapi.settings')->set('read_only', FALSE)->save(TRUE);

    // we omit title on second node to trigger an error
    $payload1 = [
      'data' => [],
      'operations' => [
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'type' => 'node--article',
            'attributes' => [
              'title' => 'JSON:API',
            ]
          ]
        ],
      ],
    ];
    $response = $this->handleOperations($payload1);
    $this->assertEquals(200, $response->getStatusCode());
    $node = Node::load(1);
    $this->assertNotNull($node);


     // we omit title on second node to trigger an error
    $payload2 = [
      'data' => [],
      'operations' => [
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'type' => 'node--article',
            'attributes' => [
              'title' => 'JSON:API',
            ]
          ]
        ],
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'attributes' => [
            ]
          ]
        ],
      ],
    ];
    $response = $this->handleOperations($payload2);
    $this->assertEquals(400, $response->getStatusCode());
    $node = Node::load(2);
    $this->assertNull($node);
    $node = Node::load(3);
    $this->assertNull($node);

  }

}
