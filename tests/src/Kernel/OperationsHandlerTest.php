<?php

namespace Drupal\Tests\jsonapi_operations\Kernel;


use Drupal\Component\Serialization\Json;
use Drupal\jsonapi\JsonApiResource\JsonApiDocumentTopLevel;
use Drupal\jsonapi_operations\Controller\OperationsHandler;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @group jsonapi_operations
 */
class OperationsHandlerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'node',
    'field',
    'jsonapi',
    'jsonapi_operations',
    'serialization',
    'system',
    'user',
  ];

  /**
   * The user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * Static UUIDs to use in testing.
   *
   * @var array
   */
  protected static $nodeUuid = [
    1 => 'b7b9731e-6e9e-4f6a-935f-4dcd58b0039a',
  ];

  /**
   * The EntityResource under test.
   *
   * @var OperationsHandler
   */
  protected $operationHandler;

  protected function setUp() {
    parent::setUp();

    // Add the entity schemas.
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    // Add the additional table schemas.
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installSchema('user', ['users_data']);

    $type = NodeType::create([
      'type' => 'article',
    ]);
    $type->save();
    $this->user = User::create([
      'name' => 'user1',
      'mail' => 'user@localhost',
      'status' => 1,
      'roles' => [],
    ]);
    $this->user->save();

    $this->node = Node::create([
      'title' => 'dummy_title',
      'type' => 'article',
      'uid' => $this->user->id(),
      'uuid' => static::$nodeUuid[1],
    ]);
    $this->node->save();

    Role::create([
      'id' => RoleInterface::ANONYMOUS_ID,
      'permissions' => [
        'access content',
        'create article content',
        'edit any article content',
        'delete any article content',
      ],
    ])->save();

    $this->operationHandler = $this->createOperationsHandler();


  }

  /**
   * Creates an instance of the subject under test.
   *
   * @return OperationsHandler
   *   An OperationsHandler instance.
   */
  protected function createOperationsHandler() {
    return new OperationsHandler(
      $this->container->get('entity_type.manager'),
      $this->container->get('jsonapi.resource_type.repository'),
      $this->container->get('database'),
      $this->container->get('http_kernel')
    );
  }

  function handleOperations($payload){
    $request = Request::create('/jsonapi/operations','PATCH', [], [], [], ['CONTENT_TYPE' => 'application/vnd.api+json'], $payload);
    return $this->operationHandler->handleOperations($request);
  }

  function testGetIndividual(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'get',
          'ref' => [
            'type' => 'node--article',
            'id' => static::$nodeUuid[1],
          ],
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
  }

  function testGetCollection(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'get',
          'ref' => [
            'type' => 'node--article'
          ],
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
  }

  function testAdd(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'type' => 'node--article',
            'attributes' => [
              'title' => 'JSON:API',
            ]
          ]
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
    $node = Node::load(count(static::$nodeUuid)+1);
    $this->assertNotNull($node);
    $this->assertEqual('JSON:API', $node->getTitle());
  }

  function testUpdate(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'update',
          'ref' => [
            'type' => 'node--article',
            'id' => static::$nodeUuid[1],
          ],
          'data' => [
            'type' => 'node--article',
            'id' => static::$nodeUuid[1],
            'attributes' => [
              'title' => 'JSON:API Operations',
            ]
          ]
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
    $node = Node::load(1);
    $this->assertNotNull($node);
    $this->assertEqual('JSON:API Operations', $node->getTitle());
  }

  function testDelete(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'delete',
          'ref' => [
            'type' => 'node--article',
            'id' => static::$nodeUuid[1],
          ],
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertFalse(Node::load(1));
  }

  function testTwoAdds(){
    $payload = Json::encode([
      'data'=>[],
      'operations' => [
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'type' => 'node--article',
            'attributes' => [
              'title' => 'JSON:API',
            ]
          ]
        ],
        [
          'op' => 'add',
          'ref' => [
            'type' => 'node--article'
          ],
          'data' => [
            'type' => 'node--article',
            'attributes' => [
              'title' => 'GraphQL',
            ]
          ]
        ],
      ],
    ]);
    $response = $this->handleOperations($payload);
    $this->assertEquals(200, $response->getStatusCode());
    $node = Node::load(count(static::$nodeUuid)+1);
    $this->assertNotNull($node);
    $this->assertEqual('JSON:API', $node->getTitle());
    $node = Node::load(count(static::$nodeUuid)+2);
    $this->assertNotNull($node);
    $this->assertEqual('GraphQL', $node->getTitle());
  }

}
